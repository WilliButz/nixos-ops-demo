#!/usr/bin/env nix-shell
#!nix-shell -i bash -p pwgen wireguard-tools

set -eu

SECRET_DIR="${SECRET_DIR:?"not set. Please specify target directory for secrets"}"

declare -a SECRETS WG_KEYPAIRS WG_PSKS

SECRETS=(
  "gitea_db_pass"
  "gitea_admin_pass"
  "nextcloud_admin_pass"
  "grafana_admin_pass"
  "grafana_secret_key"
)

WG_KEYPAIRS=(
  "host-a"
  "host-b"
)

WG_PSKS=(
  "wg_preshared_key"
)

OLD_UMASK=$(umask -p)

umask 0037

mkdir -p "$SECRET_DIR" -m 0750

# generate normal passwords / secrets
for secret in "${SECRETS[@]}"; do
    pwgen -s 42 1 > "${SECRET_DIR}/$secret"
done

# generate wireguard keypairs
for host in "${WG_KEYPAIRS[@]}"; do
    privkey="${SECRET_DIR}/wg_private_key_${host}"
    pubkey="${SECRET_DIR}/wg_public_key_${host}"

    wg genkey > "$privkey"
    wg pubkey < "$privkey" > "$pubkey"
done

# generate wireguard preshared keys
for psk in "${WG_PSKS[@]}"; do
    wg genpsk > "${SECRET_DIR}/$psk"
done

# reset umask
eval "$OLD_UMASK"
