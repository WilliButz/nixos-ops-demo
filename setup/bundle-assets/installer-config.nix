{ pkgs, ... }:

{
  environment = {
    systemPackages = with pkgs; [
      curl
      vim
      (pkgs.writeShellScriptBin "setup.sh" ''
        set -e

        sfdisk /dev/sda < ${./disk_layout}

        sleep 2

        # create swap
        mkswap /dev/disk/by-partlabel/swap
        swapon /dev/disk/by-partlabel/swap

        # create root fs
        mkfs.ext4 /dev/disk/by-partlabel/root
        mount -t ext4 /dev/disk/by-partlabel/root /mnt

        # copy config for the installer
        mkdir -p /mnt/etc/nixos/
        cp -v ${./pre-deployment-config.nix} /mnt/etc/nixos/configuration.nix
        cp -v ${./hardware-config.nix} /mnt/etc/nixos/hardware-config.nix
      '')
    ];
  };

  kexec.autoReboot = false;

  services.openssh.enable = true;

  users.users.root = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIAwfpM32DPqH61yJFx5fNsZb654saATGr4pinQsj2VI butz@workstation"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINqBP9a/Q6SWpLBZtsQ8tqC63nxvONwoarNhpcoDwnG9 butz@notebook"
    ];
  };
}
