NixOS Ops Demo
==============

This is an example deployment for a live demonstration of NixOS and NixOps, with some commonly used services and tools.

Used Tools And Services
-----------------------
* [NixOS](https://nixos.org)
* [NixOps](https://github.com/NixOS/nixops)
* [Gitea](https://gitea.io)
* [Grafana](https://grafana.com)
* [Knot Resolver](https://www.knot-resolver.cz)
* [Let's Encrypt](https://letsencrypt.org)
* [Nextcloud](https://nextcloud.com)
* [NGINX](https://nginx.org/en)
* [Prometheus](https://prometheus.io)
* [systemd-networkd](https://github.com/systemd/systemd)
* [WireGuard](https://wireguard.com)

Deployment Overview
-------------------
```
  [ Host A ]  <---- WireGuard tunnel ---->  [ Host B ]
  * Gitea                                   * NGINX w/ Let's Encrypt
  * Grafana
  * Nextcloud
  * Prometheus
```

Setup
-----
```
nixos-ops-demo
├── host-a
│  └── default.nix                      # host-a-specific config
├── host-b
│  └── default.nix                      # host-b-specific config
├── setup
│  ├── bundle-assets
│  │  ├── disk_layout                   # physical disk layout
│  │  ├── hardware-config.nix           # vm-specifc config
│  │  ├── installer-config.nix          # config for the "live image"
│  │  └── pre-deployment-config.nix     # base config for nixos-install
│  ├── generate-bundle.sh
│  ├── generate-secrets.sh
│  └── workstation-config.nix
├── base-config.nix                     # common config for host-a/b
├── demo.nix                            # NixOps deployment definition
└── README.md
```

1. Generate secrets
```bash
# in the base directory of this repository run the following
# or set a custom directory for the secrets and update the
# Nix config accordingly
SECRET_DIR=secrets ./setup/generate-secrets.sh
```
2. Add this deployment to the NixOps database
```bash
# create the deployment "demo" using the specification from "demo.nix"
$ nixops create -d demo demo.nix
created deployment ‘aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee’
aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee
```

The repository should now have the following file structure:
```
nixos-ops-demo
├── host-a
│  └── default.nix
├── host-b
│  └── default.nix
├── secrets
│  ├── gitea_admin_pass
│  ├── gitea_db_pass
│  ├── grafana_admin_pass
│  ├── grafana_secret_key
│  ├── nextcloud_admin_pass
│  ├── wg_preshared_key
│  ├── wg_private_key_host-a
│  ├── wg_private_key_host-b
│  ├── wg_public_key_host-a
│  └── wg_public_key_host-b
├── setup
│  ├── bundle-assets
│  │  ├── disk_layout
│  │  ├── hardware-config.nix
│  │  ├── installer-config.nix
│  │  └── pre-deployment-config.nix
│  ├── generate-bundle.sh
│  ├── generate-secrets.sh
│  └── workstation-config.nix
├── base-config.nix
├── demo.nix
└── README.md
```

And the deployment should also be listed after running:
```bash
$ nixops list
+--------------------------------------+------+------------------------+------------+------+
| UUID                                 | Name | Description            | # Machines | Type |
+--------------------------------------+------+------------------------+------------+------+
| aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee | demo | Unnamed NixOps network |          0 |      |
+--------------------------------------+------+------------------------+------------+------+
```

Note that the Description, Nr. of Machines and Type will be updated after the first evaluation.

Additional Resources
--------------------
- [nixos-generators](https://github.com/nix-community/nixos-generators)
