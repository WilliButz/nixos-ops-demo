{ config, lib, pkgs, ... }:

{
  networking = {
    useDHCP = false;
    useNetworkd = true;
    firewall.allowedUDPPorts = [ 52342 ];
  };

  systemd.network = {
    netdevs = {
      "10-wireguard" = {
        netdevConfig = {
          Name = "wg0";
          Kind = "wireguard";
        };
        extraConfig = ''
          [WireGuard]
          ListenPort=52342
          PrivateKeyFile=/var/secrets/wg_private_key
        '';
      };
    };
    networks = {
      "20-uplink".extraConfig = ''
        [Match]
        Type=ether

        [Link]
        RequiredForOnline=routable

        [Network]
        DHCP=yes
      '';
    };
  };

  services = {

    openssh.enable = true;

    resolved.enable = false;

    kresd = {
      enable = true;
      extraConfig = ''
        modules = {
          'hints > iterate',
        }
      '' + lib.concatStrings (lib.mapAttrsToList (hostName: addresses: ''
        hints['${hostName}'] = '${builtins.elemAt addresses 0}'
        hints['${hostName}'] = '${builtins.elemAt addresses 1}'
      '') {
        host-a-wg = [ "fc00:2305::1" "10.23.42.1" ];
        host-b-wg = [ "fc00:2305::2" "10.23.42.2" ];
      });
    };

    prometheus.exporters.node.enable = true;

  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIAwfpM32DPqH61yJFx5fNsZb654saATGr4pinQsj2VI butz@workstation"
  ];

  deployment.keys = {
    wg_preshared_key = {
      destDir = "/var/secrets";
      keyFile = secrets/wg_preshared_key;
      group = "systemd-network";
      permissions = "0640";
    };
    wg_private_key = {
      destDir = "/var/secrets";
      keyFile = secrets/wg_private_key_ + config.networking.hostName;
      group = "systemd-network";
      permissions = "0640";
    };
  };

  environment.systemPackages = [ pkgs.wireguard-tools ];

  imports = [
    setup/bundle-assets/hardware-config.nix
  ];

  systemd.services.systemd-networkd.serviceConfig.SupplementaryGroups = "keys";

  system.stateVersion = "20.09";
}
