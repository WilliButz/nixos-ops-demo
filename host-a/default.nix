{ pkgs, lib, ... }:

{
  networking.hostName = "host-a";

  systemd.network = {
    netdevs = {
      "10-wireguard".extraConfig = ''
        [WireGuardPeer]
        PublicKey=${builtins.readFile ../secrets/wg_public_key_host-b}
        AllowedIPs=fc00:2305::2/128,10.23.42.2/32
        PresharedKeyFile=/var/secrets/wg_preshared_key
        Endpoint=host-b.some-demo.site:52342
      '';
    };
    networks = {
      "20-wireguard".extraConfig = ''
        [Match]
        Name=wg0

        [Link]
        MTUBytes=1280
        RequiredForOnline=routable

        [Address]
        Address=fc00:2305::1/128
        Peer=fc00:2305::2/128

        [Address]
        Address=10.23.42.1/32
        Peer=10.23.42.2/32
      '';
    };
  };

  services = {

    gitea = {
      enable = true;
      cookieSecure = true;
      database = {
        type = "postgres";
        createDatabase = true;
        passwordFile = "/var/secrets/gitea_db_pass";
      };
      log.level = "Warn";
      appName = "Gitea";
      rootUrl = "https://git.some-demo.site";
      domain = "git.some-demo.site";
      httpPort = 3001;
      disableRegistration = true;
      settings = {
        ui.DEFAULT_THEME = "arc-green";
        picture.DISABLE_GRAVATAR = true;
        metrics.ENABLED = true;
        i18n = {
          LANGS = "en-US,de-DE";
          NAMES = "English,Deutsch";
        };
        other = {
          SHOW_FOOTER_VERSION = false;
          SHOW_FOOTER_TEMPLATE_LOAD_TIME = false;
        };
        log.MODE = "console";
      };
    };

    nextcloud = {
      enable = true;
      package = pkgs.nextcloud19;
      hostName = "cloud.some-demo.site";
      https = true;
      config = {
        adminuser = "admin";
        adminpassFile = "/var/secrets/nextcloud_admin_pass";
        overwriteProtocol = "https";
      };
    };

    prometheus = {
      enable = true;
      extraFlags = [
        "--web.page-title='Metrics - Some Demo Site'"
      ];
      scrapeConfigs = [
        {
          job_name = "gitea";
          scrape_interval = "30s";
          static_configs = lib.singleton {
            targets = [ "localhost:3001" ];
          };
        }

        {
          job_name = "nginx";
          scrape_interval = "30s";
          static_configs = lib.singleton {
            targets = [ "host-b-wg:9113" ];
          };
        }

        {
          job_name = "node";
          scrape_interval = "30s";
          static_configs = [
            {
              targets = [ "localhost:9100" ];
              labels.target = "host-a";
            }
            {
              targets = [ "host-b-wg:9100" ];
              labels.target = "host-b";
            }
          ];
        }
      ];
    };

    grafana = {
      enable = true;
      addr = "";
      analytics.reporting.enable = false;
      domain = "monitoring.some-demo.site";
      rootUrl = "https://monitoring.some-demo.site";
      security = {
        adminPasswordFile = "/var/secrets/grafana_admin_pass";
        secretKeyFile = "/var/secrets/grafana_secret_key";
      };
      extraOptions.LOG_LEVEL = "warn";
    };

  };

  networking.firewall.extraCommands = ''
    ip46tables -I nixos-fw -i wg0 -p tcp --dport 80 -m comment --comment "nextcloud" -j ACCEPT
    ip46tables -I nixos-fw -i wg0 -p tcp --dport 3000 -m comment --comment "grafana" -j ACCEPT
    ip46tables -I nixos-fw -i wg0 -p tcp --dport 3001 -m comment --comment "gitea" -j ACCEPT
  '';

  deployment.keys = {
    gitea_db_pass = {
      destDir = "/var/secrets";
      keyFile = ../secrets/gitea_db_pass;
      user = "gitea";
      group = "gitea";
    };
    gitea_admin_pass = {
      destDir = "/var/secrets";
      keyFile = ../secrets/gitea_admin_pass;
      user = "gitea";
      group = "gitea";
    };
    grafana_admin_pass = {
      destDir = "/var/secrets";
      keyFile = ../secrets/grafana_admin_pass;
      user = "grafana";
      group = "grafana";
    };
    grafana_secret_key = {
      destDir = "/var/secrets";
      keyFile = ../secrets/grafana_secret_key;
      user = "grafana";
      group = "grafana";
    };
    nextcloud_admin_pass = {
      destDir = "/var/secrets";
      keyFile = ../secrets/nextcloud_admin_pass;
      user = "nextcloud";
      group = "nextcloud";
    };
  };

  environment.shellAliases = {
    create-admin = "sudo -u gitea GITEA_WORK_DIR=/var/lib/gitea ${pkgs.gitea}/bin/gitea admin create-user --username butz --must-change-password=false --email butz@localhost --password \"$(</var/secrets/gitea_admin_pass)\"";
  };

  users.users.gitea.extraGroups = [ "keys" ];
  users.users.grafana.extraGroups = [ "keys" ];
  users.users.nextcloud.extraGroups = [ "keys" ];
}
