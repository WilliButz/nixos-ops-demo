{
  network = {
    description = "demo deployment";
    enableRollback = true;
  };

  defaults = {
    deployment.targetEnv = "none";
    imports = [ ./base-config.nix ];
  };

  host-a = {
    imports = [
      ./host-a
    ];

    deployment.targetHost = "host-a.some-demo.site";
  };
  host-b = {
    imports = [
      ./host-b
    ];

    deployment.targetHost = "host-b.some-demo.site";
  };
}
